<?php
/*
Plugin Name: My First Plugin
Plugin URI: www.kiarashshahbazi.ir
Description: This is my first plugin in wordpress
Author: Kiarash Alishahbazpoori 
Version: 1.0.0
Author URI: www.kiarashshahbazi.ir
Text Domain: myfirstplugin
*/
function showNameInWordpress($name){
    echo "My name is {$name}";
}
showNameInWordpress("Kiarash Alishahbazpoori");