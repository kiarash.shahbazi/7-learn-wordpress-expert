<?php
/*
Plugin Name: افزونه فیلتر کلمات
Plugin URI: www.kiarashshahbazi.ir
Description: یک افزونه ساده برای فیلتر محتوای وردپرس
Author: Kiarash Alishahbazpoori 
Version: 1.0.0
Author URI: www.kiarashshahbazi.ir
Text Domain: wordsFilter
Domain Path: /languages/
*/

define('WF_DIR', plugin_dir_path(__FILE__));
define('WF_URL',plugin_dir_url(__FILE__));
define('WF_INC', WF_DIR.'/inc/');

function wf_activation(){}
function wf_deactivation(){}
register_activation_hook(__FILE__,'wf_activation');
register_deactivation_hook(__FILE__,'wf_deactivation');


function wf_words_filter($content){
    $word = 'وردپرس';
    $replace = '<a target="_blank" href="http://www.7learn.com">wordpress.org</a>';
    $wordLength = mb_strlen($word);
    $content = preg_replace("/{$word}/",str_repeat('*',$wordLength),$content);
    return $content;
}


add_filter('the_content', 'wf_words_filter');