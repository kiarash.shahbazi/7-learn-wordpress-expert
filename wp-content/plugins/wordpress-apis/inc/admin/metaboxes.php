<?php

function wa_save_post_handler($post_id){
    if(isset($_POST['wa_price'])){
        update_post_meta( $post_id, 'wa_price',$_POST['wa_price'] );
    }
}

function wa_meta_box_handler($post){
    ?>
    <input type="number"
     name="wa_price"
      id="wa_price"
       placeholder="قیمت مطلب"
        value="<?php echo get_post_meta($post->ID, 'wa_price', true)  ?>">
    <?php
}


function wa_register_meta_box($post_type, $post){
    add_meta_box( 
        'wa-price-meta-box',
        'قیمت مطلب',
        'wa_meta_box_handler',
        'post',
        'normal',
        'default'
    );
}



add_action( 'add_meta_boxes', 'wa_register_meta_box', 10, 2);
add_action('save_post', 'wa_save_post_handler');