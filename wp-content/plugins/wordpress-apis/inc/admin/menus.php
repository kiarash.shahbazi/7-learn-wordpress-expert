<?php

function wa_main_menu_handler(){
    if(isset($_POST['saveBtn'])){
        if($_POST['pluginStatus']){
            update_option( 'wa_plugin_status', 1);
        }else{
            delete_option('wa_plugin_status');
        }
        $current_status = get_option( "wa_plugin_status", 0 );
    }
    include WA_PLUGIN_TPL.'admin/menus/main.php';
}

function wa_general_menu_handler(){
    include WA_PLUGIN_TPL . "admin/menus/general.php";
}

function wa_register_menus(){
    add_menu_page( 
                    "تنظیمات پلاگین",
                    "تنظیمات پلاگین",
                    "manage_options",
                    "wa_admin",
                    "wa_main_menu_handler"
                );
    add_submenu_page( 
                        "wa_admin",
                        "تنظیمات عمومی",
                        "تنظیمات عمومی",
                        "manage_options",
                        "wa_general_menu",
                        "wa_general_menu_handler" 
                    );
}

add_action( "admin_menu", "wa_register_menus");