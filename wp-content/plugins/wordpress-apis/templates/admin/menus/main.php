<div class="wrap">
    <h1>تنظیمات پلاگین</h1>
    <form action="" method="post">
        <label for="pluginStatus">
        <input type="checkbox"
         name="pluginStatus"
          id="pluginStatus"
          <?php echo isset($current_status) && intval($current_status) > 0 ? "checked" : "" ?>
          >
        فعال بودن پلاگین
        </label>
        <button name="saveBtn" type="submit" class="button button-primary">ذخیره سازی</button>
    </form>
</div>