<?php
/*
Plugin Name: wordpress-apis
Plugin URI: www.kiarashshahbazi.ir
Description: یک افزنه برای کار با ای پی آی های وردپرس
Author: Kiarash Alishahbazpoori 
Version: 1.0.0
Author URI: www.kiarashshahbazi.ir
*/

define('WA_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('WA_PLUGIN_URL',plugin_dir_url(__FILE__));
define('WA_PLUGIN_INC', WA_PLUGIN_DIR.'/inc/');
define('WA_PLUGIN_TPL', WA_PLUGIN_DIR.'/templates/');


function wa_activation_func(){}
function wa_deactivation_func(){}

register_activation_hook(__FILE__, 'wa_activation_func');
register_deactivation_hook(__FILE__, 'wa_deactivation_func');


if(is_admin()){
    include WA_PLUGIN_INC.'admin/menus.php';
    include WA_PLUGIN_INC.'admin/metaboxes.php';
}



