<?php
/*
Plugin Name: sample plugin
Plugin URI: www.kiarashshahbazi.ir
Description: a sample plugin
Author: Kiarash Alishahbazpoori 
Version: 1.0.0
Author URI: www.kiarashshahbazi.ir
Text Domain: myfirstplugin
*/
define('PLUGIN_DIR',plugin_dir_path(__FILE__));
define('PLUGIN_URL', plugin_dir_url(__FILE__));
define('PLUGIN_INC', PLUGIN_DIR.'/inc/');

function sample_plugin_activation(){}
function sample_plugin_deactivation(){}

register_activation_hook(__FILE__,'sample_plugin_activation');
register_deactivation_hook(__FILE__,'sample_plugin_deactivation');


if(is_admin()){
    include PLUGIN_INC.'admin/menus.php';
}else{
    include PLUGIN_INC.'user/menus.php';
}

include PLUGIN_INC.'common/public.php';