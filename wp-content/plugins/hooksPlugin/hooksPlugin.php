<?php
/*
Plugin Name: hook plugin
Plugin URI: www.kiarashshahbazi.ir
Description: This is my hook plugin in wordpress
Author: Kiarash Alishahbazpoori 
Version: 1.0.0
Author URI: www.kiarashshahbazi.ir
*/



/* ACTION HOOKS */


function sendEmail(){
    echo "email send!";
}
function sendSms(){
    echo "sms send!";
}
function sendFactor(){
    echo "factor send!";
}

add_action('after-payment','sendEmail');
add_action('after-payment','sendSms');
add_action('after-payment','sendFactor');

#do_action("after-payment");



/* FILTER HOOKS */

function power2($param){
    return $param*$param;
}

function mines4($param){
    return $param - 4;
}

add_filter('get-amount','power2');
add_filter('get-amount','mines4');

$result = apply_filters('get-amount',1600);

echo $result;