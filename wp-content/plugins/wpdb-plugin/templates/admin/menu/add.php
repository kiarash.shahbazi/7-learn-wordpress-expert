<div class="wrap">
    <h1>ثبت داده جدید</h1>
    <form method="post">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">نام</th>
                <td><input type="text" name="firstName"></td>
            </tr>
            <tr valign="top">
                <th scope="row">نام خانوادگی</th>
                <td><input type="text" name="lastName"></td>
            </tr>
            <tr valign="top">
                <th scope="row">موبایل</th>
                <td><input type="text" name="mobile"></td>
            </tr>
            <tr valign="top">
                <th scope="row"></th>
                <td><input type="submit" name="saveBtn" class="button" value="ثبت"></td>
            </tr>
        </table>
    </form>
</div>