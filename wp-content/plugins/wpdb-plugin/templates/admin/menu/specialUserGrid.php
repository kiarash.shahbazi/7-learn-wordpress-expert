<div class="wrap">
    <h1>کاربران ویژه</h1>
    <table class="widefat">
        <thead>
        <tr>
            <th>شناسه</th>
            <th>نام کامل</th>
            <th>ایمیل</th>
            <th>شماره همراه</th>
            <th>کیف پول</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <?php
            $user_wallet_value = get_user_meta($user->ID, "wallet", true);
            $user_wallet_value = empty($user_wallet_value) ? 0 : $user_wallet_value;
            ?>
            <tr>
                <td><?php echo $user->ID ?></td>
                <td><?php echo $user->display_name ?></td>
                <td><?php echo $user->user_email ?></td>
                <td><?php echo get_user_meta($user->ID, "mobile", true) ?></td>
                <td><?php echo number_format($user_wallet_value) . " تومان " ?></td>
                <td>
                    <a href="<?php echo add_query_arg(["action" => "edit", "id" => $user->ID]) ?>"><span
                                style="color: yellow" class="dashicons dashicons-edit"></span></a>
                    <a href="<?php echo add_query_arg(["action" => "removeMobileAndWallet", "id" => $user->ID]) ?>"><span
                                style="color: darkred" class="dashicons dashicons-trash"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>