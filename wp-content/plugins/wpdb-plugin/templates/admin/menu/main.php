<div class="wrap">
    <h1>نمایش اطلاعات</h1>
    <a href="<?php echo add_query_arg(["option"=>"add"])?>">ثبت اطلاعات</a>
    <button id="sendAjax" class="button">ارسال درخواست AJAX</button>
    <table class="widefat">
        <thead>
        <tr>
            <th>شناسه</th>
            <th>نام</th>
            <th>نام خانوادگی</th>
            <th>شماره تماس</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($results as $res): ?>
            <tr>
                <td><?php echo $res->id ?></td>
                <td><?php echo $res->firstName ?></td>
                <td><?php echo $res->lastName ?></td>
                <td><?php echo $res->mobile ?></td>
                <td><a href="<?php echo add_query_arg(['option' => 'delete', 'item' => $res->id]) ?>">حذف</a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>