<?php


function wpdb_admin_menu_handler()
{
    global $wpdb;
    $option = $_GET['option'];
    if ($option == 'delete') {
        $item_id = intval($_GET['item']);
        if ($item_id > 0) {
            $wpdb->delete($wpdb->prefix . 'sample', ['id' => $item_id]);
        }
    }
    if ($option == "add") {
        if (isset($_POST["saveBtn"])) {
            $wpdb->insert($wpdb->prefix . 'sample', [
                'firstName' => $_POST['firstName'],
                'lastName' => $_POST['lastName'],
                'mobile' => $_POST['mobile']
            ]);
        }
        include WPDB_PLUGIN_TMP . "admin/menu/add.php";
    } else {
        $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sample");
        include WPDB_PLUGIN_TMP . "admin/menu/main.php";
    }
}

function special_users_info_handler()
{
    global $wpdb;
    if (isset($_GET['action']) && $_GET['action'] == 'edit') {
        $userId = intval($_GET['id']);
        if (isset($_POST['saveBtn'])) {
            $user_mobile = $_POST['mobile'];
            $user_wallet = $_POST['wallet'];
            if (!empty($user_mobile)) {
                update_user_meta($userId, "mobile", $user_mobile);
            }
            if (!empty($user_wallet)) {
                update_user_meta($userId, "wallet", $user_wallet);
            }
        }
        $user_mobile = get_user_meta($userId, 'mobile', true);
        $user_wallet = get_user_meta($userId, 'wallet', true);
        include WPDB_PLUGIN_TMP . "admin/menu/specialUserEdit.php";
        return;
    }
    if (isset($_GET['action']) && $_GET['action'] == 'removeMobileAndWallet') {
        $userId = intval($_GET['id']);
        delete_user_meta($userId,"mobile");
        delete_user_meta($userId,"wallet");
    }
    $users = $wpdb->get_results("SELECT ID,user_email,display_name FROM {$wpdb->users}");
    include WPDB_PLUGIN_TMP . "admin/menu/specialUserGrid.php";
}


function register_admin_menu_wpdb_plugin()
{
    add_menu_page(
        "پلاگین سفارشی",
        "پلاگین سفارشی",
        "manage_options",
        "wpdb_admin_menu",
        "wpdb_admin_menu_handler"
    );

    add_submenu_page(
        "wpdb_admin_menu",
        "کاربران ویژه",
        "کاربران ویژه",
        "manage_options",
        "special_users_info",
        "special_users_info_handler"
    );
}


add_action('admin_menu', 'register_admin_menu_wpdb_plugin');