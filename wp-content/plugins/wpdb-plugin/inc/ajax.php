<?php

add_action('wp_ajax_calculate_operation','wp_calculate_data_recived_by_ajax');

function wp_calculate_data_recived_by_ajax(){
    $num1 = $_POST['number1'];
    $num2 = $_POST['number2'];

    wp_send_json([
        'success' => true,
        'result' => $num1 + $num2
    ]);

}

