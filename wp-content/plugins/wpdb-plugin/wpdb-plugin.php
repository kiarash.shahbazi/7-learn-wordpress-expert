<?php
/*
Plugin Name: wpdb-plugin
Plugin URI: www.kiarashshahbazi.ir
Description: یک افزنه برای کار با دیتابیس وردپرس
Author: Kiarash Alishahbazpoori
Version: 1.0.0
Author URI: www.kiarashshahbazi.ir
*/

define('WPDB_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('WPDB_PLUGIN_URL', plugin_dir_url(__FILE__));
define('WPDB_PLUGIN_INC', WPDB_PLUGIN_DIR."/inc/");
define('WPDB_PLUGIN_TMP', WPDB_PLUGIN_DIR."/templates/");

function register_activation_func(){}
function register_deactivation_func(){}

register_activation_hook(__FILE__,'register_activation_func');
register_deactivation_hook(__FILE__,'register_deactivation_func');


if (is_admin()){
    include WPDB_PLUGIN_INC . "admin/menu.php";
}
include WPDB_PLUGIN_INC . "ajax.php";

function registerStyleInWpdbPlugin(){
    wp_register_style("main-style-wpdb-plugin", WPDB_PLUGIN_URL."assets/css/main.css");
    wp_enqueue_style("main-style-wpdb-plugin");
}

function registerScriptsInWpdbPlugin(){

    if (is_admin()){
        wp_register_script("main-script-wpdb-plugin-admin", WPDB_PLUGIN_URL."assets/js/main-admin.js",["jquery"],"1.0.0",true);
        wp_enqueue_script("main-script-wpdb-plugin-admin");
    }else{
        wp_register_script("main-script-wpdb-plugin-front", WPDB_PLUGIN_URL."assets/js/main-front.js",["jquery"],"1.0.0",true);
        wp_enqueue_script("main-script-wpdb-plugin-front");
    }
}

add_action("wp_enqueue_scripts","registerStyleInWpdbPlugin");
add_action("admin_enqueue_scripts","registerStyleInWpdbPlugin");

add_action("wp_enqueue_scripts","registerScriptsInWpdbPlugin");
add_action("admin_enqueue_scripts","registerScriptsInWpdbPlugin");

