<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress.exp' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=_oD7JxJ:z5^Jf2^fwtE3_+|D<=FtB./nXE=3}WdH3U=?G<pY,`Fe#!2:Nj1J=}L' );
define( 'SECURE_AUTH_KEY',  ',%<p4nlEdZ@BTo2#YqhTJ?}lsykVptF<q(!&U+ZV`z=OI<]s`{4o|JW<FsUqpq)V' );
define( 'LOGGED_IN_KEY',    'EE39U%D%&3Xo7Z=_VFr6M0$GfXFtZ`DK8bGiMEZA$Z6Af/Iv~Wi |h`Ka.%J:or ' );
define( 'NONCE_KEY',        'g*cmH#p=syD+Ib8_pM^ W[II9T&lwMcN9qlqs655B0Uyzyah/mpR<:Ghw(Zl4GRI' );
define( 'AUTH_SALT',        ';#uk@$PkBp^^?%e%$T=7[CRzrWFAH_*FbJk: ]P}ts>ZrCYe&2.8Hq1}j0rE_]|=' );
define( 'SECURE_AUTH_SALT', 'v1eSAgo_@sD;r*3Z]Q<+/7bS+P$M`Pak)b/z<g-T%9~1]q7]544ekg! 3mt[EC1^' );
define( 'LOGGED_IN_SALT',   'dehC4ApOct},Y[ c1172Q*9!SGw;!7M8a8AIXSc<749s2%r6AP8MT-63ZgTW*D@<' );
define( 'NONCE_SALT',       'M7:}4]bL09Uo?B>q8>KiOy6^s5z>i04//mT2?pW(oK]O{R23AP?{8^PT&]&Lp>nD' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'exp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
